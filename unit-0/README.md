![header](https://i.imgur.com/3OKO4ov.png)

# Unit 0: Introduction

This unit is mostly for administrative purposes. 

## Contents:

### Lesson 0.0: Syllabus 

This lesson outlines the expectations and consequences.

__Launch Lesson 0.0: Syllabus below:__

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/simonhasan/intro-to-cs-notebooks/master?filepath=unit-0%2Flesson-0.0-syllabus.ipynb)